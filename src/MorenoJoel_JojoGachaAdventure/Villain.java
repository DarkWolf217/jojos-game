package MorenoJoel_JojoGachaAdventure;

/**
 * Clase que contiene todas las variables del villano
 * (no está en uso)
 * @author Joel Moreno
 *
 */
public class Villain extends Character implements levelUp {
	/**
	 * Constructor del villano
	 */
	public Villain(String name, int x1, int y1, String sprites, String charName, String standName,
			int maxHP, int actualHP, int atk) {
		super(name, x1, y1, sprites, charName, standName, maxHP, actualHP, atk);
		
	}
	/**
	 * Función que calcula la xp que suelta el villano al ser derrotado
	 */
	@Override
	public int gainExp() {
		
		int xp = Math.abs(this.maxHP/this.atk);		
		System.out.println("Xp obtenida es "+xp);
		return xp;
		
	}

	@Override
	public void level(int obtainedXP) {
		
		
	}
	
}
