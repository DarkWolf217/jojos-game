package MorenoJoel_JojoGachaAdventure;

public interface levelUp {
	/**
	 * Función que calcula la xp que suelta el villano al ser derrotado
	 */
	int gainExp();
	/**
	 * Función para subir de nivel
	 */
	void level(int obtainedXP) throws InterruptedException;

}
