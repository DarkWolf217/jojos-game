package MorenoJoel_JojoGachaAdventure;

import Core.Sprite;

public class Character extends Sprite {

	/**
	 * Nombre del personaje
	 */
	String charName;
	/**
	 * Nombre del stand
	 */
	String standName;
	
	/**
	 * Vida máxima
	 */
	int maxHP;
	/**
	 * Vida actual
	 */
	int actualHP;
	
	/**
	 * Ataque del personaje
	 */
	int atk;
	
	/**
	 * Tamaño del sprite eje x 
	 */
	static int xSize = 150;
	/**
	 * Tamaño del sprite eje y
	 */
	static int ySize = 150;
	/**
	 * Para reescalar el sprite
	 */
	static float resize = 1.5f;
	
	/**
	 * Constructor de los personajes
	 */
	public Character(String name, int x1, int y1, String sprites, String charName, String standName,
			int maxHP, int actualHP, int atk) {
		super(name, x1, y1, (int)(x1+xSize*resize), (int)(y1+ySize*resize), sprites);
		this.charName = charName;
		this.standName = standName;
		this.maxHP = maxHP;
		this.actualHP = actualHP;
		this.atk = atk;
	}
	
	
	
}
