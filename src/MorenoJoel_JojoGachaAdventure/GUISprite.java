package MorenoJoel_JojoGachaAdventure;

import Core.Sprite;

/**
 * Grid
 * @author Joel Moreno
 */
public class GUISprite extends Sprite {

	/**
	 * Constructor de Grid
	 * @param x1 
	 * @param y1
	 * @param xSize
	 * @param ySize
	 */
	public GUISprite(int x1, int y1, int xSize, int ySize) {
		super("", x1, y1, x1+xSize, y1+ySize, "res/red.jpg");
		
	}	
}
