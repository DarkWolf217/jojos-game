package MorenoJoel_JojoGachaAdventure;

import Core.Sprite;

/**
 * Para poner textos en pantalla
 * 
 * @author Joel Moreno
 *
 */
public class Text extends Sprite {
	
	/**
	 * Constructor de Text
	 * @param name nombre interno del texto
	 * @param x1 posición x del texto
	 * @param y1 posición y del texto
	 * @param sprites String con el texto el cual quieres mostrar
	 */
	public Text(String name, int x1, int y1, String sprites) {
		super(name, x1, y1, x1, y1, sprites);
		this.text = true;
		
	}
	

	
}
