package MorenoJoel_JojoGachaAdventure;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import Core.Field;
import Core.Sprite;
import Core.Window;

/**
 * La clase principal del juego
 * @author Joel Moreno
 *
 */
public class MenuJojo {
	/**
	 * Campo de pixeles
	 */
	public static Field f = new Field();
	/**
	 * Ventana
	 */
	public static Window w = new Window(f);
	/**
	 * Lista de sprites
	 */
	private static ArrayList<Sprite> toDraw = new ArrayList<Sprite>();
	
	/**
	 * Boolean del bucle principal
	 */
	protected static boolean exit = false;
	
	/**
	 * Main del menú
	 * @throws InterruptedException
	 * @throws IOException 
	 */
	public static void main(String[] args) throws InterruptedException, IOException {		
		while (!exit) {
			fondo();
			
			if (w.getPressedKeys().contains('1')) {
				farm();
			}else if(w.getPressedKeys().contains('2')) {
				boss();
			}else if(w.getPressedKeys().contains('0')){
				exit=true;
			}
			Thread.sleep(66);
		}
		fichero();
		w.close();
		System.exit(0);
	}
	
	/**
	 * Creación de ficheros para guardar el nivel, experiencia, victorias y derrotas
	 */
	private static void fichero() throws IOException {
		
		if (Jotaro.getInstance()!=null) {			
			try {
				File file = new File("src/results.txt");
				file.createNewFile();			
				
				BufferedWriter bw = new BufferedWriter(new FileWriter("src/results.txt"));
				bw.write("Jotaro level: "+Jotaro.getInstance().level);
				bw.newLine();
				bw.write("Jotaro XP: "+Jotaro.getInstance().actualXP + "/" + Jotaro.getInstance().maxXP);
				bw.newLine();
				bw.write("Victories: "+(FarmPart.wins+BossPart.wins));
				bw.newLine();
				bw.write("Defeated bosses: "+BossPart.fboss);
				bw.newLine();
				bw.write("Deaths: "+(FarmPart.deaths+BossPart.deaths));
				bw.newLine();
				bw.flush();
				bw.close();
			} catch (Exception e) {
				System.out.println("no workea esto "+e);
			}        
		}
		
	}

	/**
	 * Para que se vea el fondo en el menú y la canción
	 */
	private static void fondo() {
		toDraw = spritesToDraw();
		w.playMusic("res/SonoChinoSadame.wav");
		f.draw(toDraw);	
		
	}

	/**
	 * Sprites del menú
	 */
	private static ArrayList<Sprite> spritesToDraw() {
		ArrayList<Sprite> sprites = new ArrayList<Sprite>();		
		sprites.add(new Sprite("City", 0, 0, w.getWidth(), w.getHeight(), "res/Sprites/jojoInicio2.png"));
		
		return sprites;
	}
	
	/**
	 * Función donde está la batalla contra el boss
	 */
	private static void boss() throws InterruptedException {
		
		BossPart.jugar();
	}	

	/**
	 * Función donde está el nivel de farmeo
	 */
	private static void farm() throws InterruptedException {
		FarmPart.jugar();
	}
	
}
