package MorenoJoel_JojoGachaAdventure;

import java.awt.Font;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;

import Core.Field;
import Core.Sprite;
import Core.Window;

/**
 * Toda la parte de acción
 * @author Joel Moreno
 */
public class FarmPart {
		
	/**
	 * Lista para dibujar los sprites
	 */
	private static ArrayList<Sprite> toDraw = new ArrayList<Sprite>();
	
	/**
	 * Jotaro
	 */
	private static Jotaro jotaro;
	/**
	 * Los sprites de Jotaro
	 */
	private static String[] jotaroSprites = {"", "res/Sprites/Jotaro&SP.gif", "res/Sprites/JotaroOraOra.gif", "res/Sprites/JotaroDonut2.gif", "res/Sprites/JotaroYarou.gif", "res/Sprites/JotaroPoints.gif", "res/Sprites/JotaroCoolPose.gif"};
	//private static StandUser avdol;
	/**
	 * Dio
	 */
	private static Villain dio;
	/**
	 * Los sprites de Dio
	 */
	private static String[] dioSprites = {"", "res/Sprites/Dio2.gif", "res/Sprites/DioDonut2.gif", "res/Sprites/DioMudaMuda2.gif", "res/Sprites/DioLaser.gif", "res/Sprites/DioRecoversHP.gif", "res/Sprites/DioCargando.gif", "res/Sprites/DioPoses.gif", "res/Sprites/DioWry.gif"};
	
	/**
	 * Texto que aparece en pantalla si ganas
	 */
	private static Text winTxt;
	/**
	 * Texto que aparece en apntalla si pierdes
	 */
	private static Text loseTxt;
	/**
	 * Vida de Jotaro que aparece en pantalla 
	 */
	private static Text jotaroHPTxt;
	/**
	 * XP de Jotaro que aparece en pantalla 
	 */
	private static Text jotaroXPTxt;
	/**
	 * Vida de Dio que aparece en pantalla
	 */
	private static Text dioHPTxt;
	/**
	 * Los diferentes ataques que puedes hacer que se muestran en pantalla
	 */
	private static Text opciones;
	/**
	 * La opción para ponerte en guardia 
	 */
	private static Text guardOption;
	/**
	 * La opción para entrar en modo furia  
	 */
	private static Text furyOption;
	/**
	 * Una vez activada la guardia ,se te indica que está activada mediante
	 * un texto en azul debajo de tu vida
	 */
	private static Text guardTxt;
	/**
	 * Una vez activada la furia ,se te indica que está activada mediante
	 * un texto en rojo debajo de tu vida
	 */
	private static Text furyTxt;
	/**
	 * Cuando usas el Ora Ora Ora, el texto cambia a gris y no puedes
	 * volver a usarlo hasta dentro de unos turnos
	 */
	private static Text OraOraTxt;
	/**
	 * Level up
	 */
	private static Text levelUp;
	/**
	 * El daño que se muestra en pantalla cuando te hieren
	 */
	private static Text damageInflictedToJOT;
	/**
	 * El daño que se muestra en pantalla cuando hieres a Dio
	 */
	private static Text damageInflictedToDIO;
	/**
	 * El daño con crítico que se muestra en pantalla cuando hieres a Dio
	 */
	private static Text damageInflictedToDIOCrit;
	/**
	 * El daño que se muestra en pantalla cuando Dio recupera vida
	 */
	private static Text dioRecoversHP;
	
	/**
	 * Multiplicador que se usa a la hora de calcular el daño recibido
	 */
	private static double guard = 1;
	/**
	 * Multiplicador que se usa a la hora de calcular el daño hecho
	 */
	private static float fury = 1;
	
	/**
	 * Contador que se usa para calcular el cooldown de la guardia
	 */
	private static int guardCont = 0;
	/**
	 * Contador que se usa para calcular el cooldown de la furia
	 */
	private static int furyCont = 0;
	
	/**
	 * Boolean que se usa para saber si has usado la guardia
	 */
	private static boolean guardUses = true;
	/**
	 * Boolean que se usa para saber si has usado la furia
	 */
	private static boolean furyUses = true;
	/**
	 * Booleano para el if del contador de guardia
	 */
	private static boolean guardBool = false;
	/**
	 * Booleano para el if del contador de furia
	 */
	private static boolean furyBool = false;	
	/*
	 * Contador para el cooldown del ataque Ora Ora Ora 
	 */
	private static int OraOraCooldown = 3;
	/**
	 * Booleano que se usa en el cooldown del ataque Ora Ora Ora
	 */
	private static boolean OraOraCooldownBool = false;
	/**
	 * Boolean para controlar si sigues en el nivel o no
	 */
	private static boolean exit = true;
	/**
	 * Contador de victorias
	 */
	static int wins = 0;
	/**
	 * Creación de muertes
	 */
	static int deaths = 0;
	/**
	 * El propio juego
	 * @throws InterruptedException 
	 */
	public static void jugar() throws InterruptedException {
		initialitate();
		battle();
		
	}
	
	/**
	 * El bucle con la batalla
	 * @throws InterruptedException
	 */
	public static void battle() throws InterruptedException {
		exit = true;
		toDraw = spritesToDraw();		
		MenuJojo.w.playMusic("res/VirtuousPope.wav");
		MenuJojo.f.draw(toDraw);
		if (exit) {
			intro();	    
		}
		while(exit) {
			
			batalla();
			exit = comprobarStatus(exit);			
			
			MenuJojo.f.draw(toDraw);
			Thread.sleep(66);
		}
		MenuJojo.f.clear();
	}
	
	
	/**
	 * Intro del nivel
	 */
	private static void intro() throws InterruptedException {
		Thread.sleep(1000);
		dio.changeImage(dioSprites[7]);
		MenuJojo.w.playSFX("res/DioSounds/S_DIO_Dialog.wav");
		Thread.sleep(4500); 
		jotaro.changeImage(jotaroSprites[5]);
		MenuJojo.w.playSFX("res/JotaroSounds/S_JOT_Teme.wav");
		Thread.sleep(2000); 
		jotaro.changeImage(jotaroSprites[1]);
		dio.changeImage(dioSprites[1]);
		
	}
	
	/**
	 * Función que comprueba el estado de la partida
	 * @param pipo boolean para saber si has ganado, perdido o si no ha terminado aún la partida
	 * @return pipo
	 * @throws InterruptedException
	 */
	private static boolean comprobarStatus(boolean pipo) throws InterruptedException {
		
		if (dio.actualHP <= 0) {
			wins++;
			int levelAux = jotaro.level;
			int obtainedXP = dio.gainExp();
			jotaro.level(obtainedXP);
			
			int aux = (int) (jotaro.maxHP*0.2);
			if (jotaro.actualHP <= jotaro.maxHP) {
				jotaro.actualHP+=aux;
				if (jotaro.actualHP>jotaro.maxHP) {
					jotaro.actualHP = jotaro.maxHP;
				}
			}			
			
			changeText(jotaroHPTxt, "Jotaro HP: "+ jotaro.actualHP + "/" + jotaro.maxHP);
			changeText(jotaroXPTxt, "Level "+ jotaro.level + " | " +"Jotaro XP: "+ jotaro.actualXP + "/" + jotaro.maxXP);
			
			if (jotaro.level > levelAux) {
				changeText(levelUp, "LEVEL UP!!");
				Thread.sleep(1500);
				changeText(levelUp, "");
			}
			
			dio.maxHP*= 1.2;
			dio.actualHP = dio.maxHP;
			dio.atk*=1.15; 
			changeText(dioHPTxt, "DIO HP: "+ dio.actualHP + "/" + dio.maxHP);
			
			return pipo;
		}else if (jotaro.actualHP <= 0){
			deaths++;
			jotaro.changeImage(jotaroSprites[0]);
			changeText(loseTxt, "YOU LOSE");
			MenuJojo.w.playSFX("res/DioSounds/S_DIO_Laughs2.wav");
			dio.changeImage(dioSprites[8]);
			Thread.sleep(5000);
			return !pipo;
		}else {
			return pipo;
		}		
		
	}

	/**
	 * El turno del enemigo
	 * @throws InterruptedException
	 */
	private static void enemy() throws InterruptedException {
		
		if (dio.actualHP <= 0) {
			
		}else {
			Random r = new Random();
			int numAtaque = r.nextInt(6);
			int dioLaser = r.nextInt(8)+dio.atk;
			int dioMuda = (int) (r.nextInt(12)+dio.atk*1.4);
			int dioMudaMuda = (int) (r.nextInt(20)+dio.atk*1.8);
			
			if (numAtaque == 0) {
				dio.changeImage(dioSprites[3]);
				MenuJojo.w.playSFX("res/dioSounds/S_DIO_MudaMuda.wav");
				Thread.sleep(2000); 
				jotaro.actualHP = (int) (jotaro.actualHP - (dioMudaMuda*guard)) < 0 ? 0 : (int) (jotaro.actualHP - (dioMudaMuda*guard));
				String aux = (dioMudaMuda*guard)+"";
				dio.changeImage(dioSprites[1]);
				changeText(jotaroHPTxt, "Jotaro HP: "+ jotaro.actualHP + "/" + jotaro.maxHP);
				changeText(damageInflictedToJOT, "-"+aux.substring(0, aux.indexOf(".")));
				Thread.sleep(1500); 
				changeText(damageInflictedToJOT, "");
			}else if (numAtaque == 1 || numAtaque == 2) {
				Thread.sleep(1000); 
				dio.changeImage(dioSprites[2]);
				MenuJojo.w.playSFX("res/dioSounds/S_DIO_Muda.wav");
				Thread.sleep(1000); 
				jotaro.actualHP = (int) (jotaro.actualHP - (dioMuda*guard)) < 0 ? 0 : (int) (jotaro.actualHP - (dioMuda*guard));
				String aux = (dioMuda*guard)+"";
				dio.changeImage(dioSprites[1]);
				changeText(jotaroHPTxt, "Jotaro HP: "+ jotaro.actualHP + "/" + jotaro.maxHP);
				changeText(damageInflictedToJOT, "-"+aux.substring(0, aux.indexOf(".")));
				Thread.sleep(1500); 
				changeText(damageInflictedToJOT, "");
			}else if (numAtaque == 3 || numAtaque == 4) {
				Thread.sleep(1000); 
				dio.changeImage(dioSprites[4]);
				Thread.sleep(600); 
				MenuJojo.w.playSFX("res/dioSounds/S_DIO_00035.wav");
				Thread.sleep(700); 
				jotaro.actualHP = (int) (jotaro.actualHP - (dioLaser*guard)) < 0 ? 0 : (int) (jotaro.actualHP - (dioLaser*guard));
				String aux = (dioLaser*guard)+"";
				dio.changeImage(dioSprites[1]);
				changeText(jotaroHPTxt, "Jotaro HP: "+ jotaro.actualHP + "/" + jotaro.maxHP);
				changeText(damageInflictedToJOT, "-"+aux.substring(0, aux.indexOf(".")));
				Thread.sleep(1500); 
				changeText(damageInflictedToJOT, "");
			}else if (numAtaque == 5) {
				int hpRecuperada = 20;
				Thread.sleep(1000); 
				dio.changeImage(dioSprites[5]);
				MenuJojo.w.playSFX("res/dioSounds/S_DIO_00023.wav");
				Thread.sleep(700);
				MenuJojo.w.playSFX("res/dioSounds/S_DIO_00023.wav");
				Thread.sleep(700); 
				MenuJojo.w.playSFX("res/dioSounds/S_DIO_00023.wav");
				Thread.sleep(700); 
				MenuJojo.w.playSFX("res/dioSounds/S_DIO_00031.wav");
				Thread.sleep(2000); 
				MenuJojo.w.playSFX("res/dioSounds/S_DIO_Laughs.wav");
				Thread.sleep(3000); 
				if (dio.actualHP+hpRecuperada <= dio.maxHP) {
					dio.actualHP = dio.actualHP+hpRecuperada;
				}else {
					dio.actualHP = dio.maxHP;
				}
				dio.changeImage(dioSprites[1]);
				changeText(dioHPTxt, "DIO HP: "+ dio.actualHP + "/" + dio.maxHP);
				changeText(dioRecoversHP, "+"+hpRecuperada);
				Thread.sleep(1500); 
				changeText(dioRecoversHP, "");
			}
		}
		
	}


	/**
	 * Tu turno
	 * @throws InterruptedException
	 */
	public static void batalla () throws InterruptedException {
		
		Random r = new Random();
		int crit = r.nextInt(10)+1;
		int jotaroOraOra = (int) (r.nextInt(15)+jotaro.atk*1.5);
		int jotaroOra = r.nextInt(7)+jotaro.atk;

		if (guardCont == 2) {
			guard = 1;
			changeText(guardTxt, "");
			guardBool = false;
		}
		if (furyCont == 2) {
			fury = 1;
			changeText(furyTxt, "");
			furyBool = false;
		}
		if (OraOraCooldown > 2) {
			OraOraTxt.textColor = 0xFFFFFF;
		}
		
		
		boolean selec = false;
		while(!selec) {
			if (MenuJojo.w.getPressedKeys().contains('1') && !OraOraCooldownBool) {
				selec=true;
				OraOraCooldownBool = true;
				jotaro.changeImage(jotaroSprites[2]);
				MenuJojo.w.playSFX("res/JotaroSounds/S_JOT_OraOra.wav");
				Thread.sleep(2000);
				if (crit == 1) {
					dio.actualHP = (int) (dio.actualHP - ((jotaroOraOra*fury)*2)) < 0 ? 0 : (int) (dio.actualHP - ((jotaroOraOra*fury)*2));
					String aux = (((jotaroOraOra*fury)*2)+"");
					changeText(damageInflictedToDIOCrit, "-CRIT "+aux.substring(0, aux.indexOf(".")));
				}else {
					dio.actualHP = (int) (dio.actualHP - (jotaroOraOra*fury)) < 0 ? 0 : (int) (dio.actualHP - ((jotaroOraOra*fury)));
					String aux = ((jotaroOraOra*fury)+"");
					changeText(damageInflictedToDIO, "-"+aux.substring(0, aux.indexOf(".")));
				}
				jotaro.changeImage(jotaroSprites[1]);
				changeText(dioHPTxt, "DIO HP: "+ dio.actualHP + "/" + dio.maxHP);
				OraOraTxt.textColor = 0xB0BEC5;
				
				Thread.sleep(1500); 
				changeText(damageInflictedToDIO, "");
				changeText(damageInflictedToDIOCrit, "");
				

			}else if (MenuJojo.w.getPressedKeys().contains('2')) {
				selec=true;
				jotaro.changeImage(jotaroSprites[3]);
				MenuJojo.w.playSFX("res/JotaroSounds/S_JOT_Ora2.wav");
				Thread.sleep(900); 
				if (crit == 1) {
					dio.actualHP = (int) (dio.actualHP - ((jotaroOra*fury)*2)) < 0 ? 0 : (int) (dio.actualHP - ((jotaroOra*fury)*2));
					String aux = (((jotaroOra*fury)*2)+"");
					changeText(damageInflictedToDIOCrit, "CRIT -"+aux.substring(0, aux.indexOf(".")));
				}else {
					dio.actualHP = (int) (dio.actualHP - (jotaroOra*fury)) < 0 ? 0 : (int) (dio.actualHP - ((jotaroOra*fury)));
					String aux = ((jotaroOra*fury)+"");
					changeText(damageInflictedToDIO, "-"+aux.substring(0, aux.indexOf(".")));
				}
				jotaro.changeImage(jotaroSprites[1]);
				changeText(dioHPTxt, "DIO HP: "+ dio.actualHP + "/" + dio.maxHP);
				
				Thread.sleep(1500); 
				changeText(damageInflictedToDIO, "");
				changeText(damageInflictedToDIOCrit, "");
				
				
			}else if (MenuJojo.w.getPressedKeys().contains('3')) {
				if (guardUses) {
					selec=true;
					guard = 0.5;
					guardBool = true;
					changeText(guardTxt, "Guard Activated");
					jotaro.changeImage(jotaroSprites[4]);
					MenuJojo.w.playSFX("res/JotaroSounds/S_JOT_Yarou.wav");
					Thread.sleep(5500); 
					MenuJojo.w.playSFX("res/JotaroSounds/S_JOT_Dio!.wav");
					Thread.sleep(1000);
					jotaro.changeImage(jotaroSprites[1]);
					guardOption.textColor = 0xB0BEC5;
					
				
					guardUses = false;
				}
			}else if (MenuJojo.w.getPressedKeys().contains('4')) {
				if (furyUses) {
					selec=true;
					fury = 2.5f;
					furyBool = true;
					changeText(furyTxt, "Fury Activated");
					jotaro.changeImage(jotaroSprites[5]);
					MenuJojo.w.playSFX("res/JotaroSounds/S_JOT_Yareyare.wav");
					Thread.sleep(4000);
					jotaro.changeImage(jotaroSprites[1]);
					furyOption.textColor = 0xB0BEC5;
					
					
					furyUses = false;
				}
			}else if (MenuJojo.w.getPressedKeys().contains('9')) {
				selec=true;
				exit = false;
			}
		}
		if (exit) {		
			enemy();
			System.out.println(OraOraCooldown);
			if(OraOraCooldownBool) {
				if(OraOraCooldown == 3) {
					OraOraCooldown = 0;
				}else if(OraOraCooldown == 2){
					OraOraCooldownBool = false;
					OraOraCooldown++;
				}else {
					OraOraCooldown++;
				}
			}
			System.out.println(OraOraCooldown);
			
			if (guardBool) {
				guardCont++;
			}
			if (furyBool) {
				furyCont++;
			}
		}		
		
	}
 
	

	/**
	 * Donde están declarados todos los sprites y tal
	 * @return
	 */
	private static ArrayList<Sprite> spritesToDraw() {
		ArrayList<Sprite> sprites = new ArrayList<Sprite>();
		
		//Draw order
		// Background
		sprites.add(new Sprite("City", 0, 0, MenuJojo.w.getWidth(), MenuJojo.w.getHeight(), "res/Sprites/jojoBGD.png"));
		
		
		//personajes
		//maxHP, actualHP y atk
		jotaro = Jotaro.getInstance("SU_Jotaro", MenuJojo.f.getWidth()/30*6-Jotaro.xSize/2, MenuJojo.f.getHeight()-MenuJojo.f.getHeight()/30*20, jotaroSprites[1], "Jotaro", "Star Platinum", 100, 100, 10);
		sprites.add(jotaro);

		dio = new Villain("V_DIO", MenuJojo.f.getWidth()/30*23-Villain.xSize/2, MenuJojo.f.getHeight()-MenuJojo.f.getHeight()/50*34, dioSprites[1], "DIO", "The World", 10, 10, 5);
		sprites.add(dio);
		
		//foreground
		sprites.add(new Sprite("Botoneria", 0, 0, MenuJojo.w.getWidth(), MenuJojo.w.getHeight(), "res/Sprites/jojoMenuSelection2.png"));
		
		
		//{{ GRIDS
		//int linesY =30, linesX = 30;
		
//		for (int i = 1; i <= linesX; i++) {
//			GUISprite aux = new GUISprite(0,(MenuJojo.MenuJojo.f.getHeight()/linesX)*i,MenuJojo.MenuJojo.f.getWidth(),2);
//			System.out.println(aux.x1+" "+aux.y1+" : "+aux.x2+" "+aux.y2);
//			sprites.add(aux);
//		}
//		for (int i = 1; i <= linesY; i++) {
//			GUISprite aux = new GUISprite((MenuJojo.MenuJojo.f.getWidth()/linesY)*i,0,2,MenuJojo.MenuJojo.f.getHeight());
//			System.out.println(aux.x1+" "+aux.y1+" : "+aux.x2+" "+aux.y2);
//			sprites.add(aux);
//
//		}
		//}}
		
		
		
		// TEXTO
		Font fuente = new Font("Consolas", Font.PLAIN, 24);
		Font crit = new Font("Consolas", Font.BOLD, 24);
		Font winLose = new Font("Consolas", Font.PLAIN, 60);
		int white = 0xFFFFFF;
		int green = 0x7CEA55;
		int orange = 0xEC8A2E;
		int red = 0x922B21;
		int redCrit = 0xE74C3C;
		int blue = 0x2874A6;
		
		winTxt = new Text("Win", 500, 300, "");
		winTxt.textColor = white;
		winTxt.font = winLose;
		sprites.add(winTxt);
		
		loseTxt = new Text("Lose", 500, 300, "");
		loseTxt.textColor = white;
		loseTxt.font = winLose;
		sprites.add(loseTxt);
		
		jotaroHPTxt = new Text("VidaDeJotaro", 40, 550, "Jotaro HP: "+jotaro.actualHP + "/" + jotaro.maxHP);
		jotaroHPTxt.textColor = white;
		jotaroHPTxt.font = fuente;
		sprites.add(jotaroHPTxt);
		
		jotaroXPTxt = new Text("XPDeJotaro", 40, 580, "Level "+ jotaro.level + " | " +"Jotaro XP: "+ jotaro.actualXP + "/" + jotaro.maxXP);
		jotaroXPTxt.textColor = white;
		jotaroXPTxt.font = fuente;
		sprites.add(jotaroXPTxt);
		
	    dioHPTxt = new Text("VidaDeDio", 830, 550, "DIO HP: "+dio.actualHP + "/" + dio.maxHP);
		dioHPTxt.textColor = white;
		dioHPTxt.font = fuente;
		sprites.add(dioHPTxt);
		
		OraOraTxt = new Text("OraDonut", 480, 550, "1.Ora Ora Ora");
		OraOraTxt.textColor = white;
		OraOraTxt.font = fuente;
		sprites.add(OraOraTxt);
		
		opciones = new Text("OraOraOra", 480, 580, "2.Just one Ora");
		opciones.textColor = white;
		opciones.font = fuente;
		sprites.add(opciones);
		
		guardOption = new Text("ReduccionDeDaño", 480, 610, "3.Guard (+50% def)");
		guardOption.textColor = white;
		guardOption.font = fuente;
		sprites.add(guardOption);
		
		furyOption = new Text("AumentoDeDaño", 480, 640, "4.Fury (+150% atk)");
		furyOption.textColor = white;
		furyOption.font = fuente;
		sprites.add(furyOption);
		
		guardTxt = new Text("Guard", 40, 610, "");
		guardTxt.textColor = blue;
		guardTxt.font = fuente;
		sprites.add(guardTxt);
		
		furyTxt = new Text("Fury", 40, 640, "");
		furyTxt.textColor = red;
		furyTxt.font = fuente;
		sprites.add(furyTxt);
		
		levelUp = new Text("Level Up", 360, 280, "");
		levelUp.textColor = green;
		levelUp.font = fuente;
		sprites.add(levelUp);
		
		damageInflictedToJOT = new Text("DañoHechoAJotaro", 360, 280, "");
		damageInflictedToJOT.textColor = orange;
		damageInflictedToJOT.font = fuente;
		sprites.add(damageInflictedToJOT);
		
		damageInflictedToDIO = new Text("DañoHechoADio", 880, 280, "");
		damageInflictedToDIO.textColor = orange;
		damageInflictedToDIO.font = fuente;
		sprites.add(damageInflictedToDIO);
		
		damageInflictedToDIOCrit = new Text("DañoHechoADioConCrit", 825, 280, "");
		damageInflictedToDIOCrit.textColor = redCrit;
		damageInflictedToDIOCrit.font = crit;
		sprites.add(damageInflictedToDIOCrit);
		
		dioRecoversHP = new Text("VidaRecuperadaDio", 900, 280, "");
		dioRecoversHP.textColor = green;
		dioRecoversHP.font = fuente;
		sprites.add(dioRecoversHP);
		
		return sprites;
	}

	/**
	 * Función que no permite la reescala de la ventana
	 */
	private static void initialitate() {
		MenuJojo.w.setResizable(false);
	}
	
	/**
	 * Función que permite el cambio de los textos en pantalla
	 * @param text hace referencia a la clase texto 
	 * @param newText es el texto el cual cambias
	 */
	private static void changeText(Text text, String newText) {
		text.changeImage(newText);
	}
}
