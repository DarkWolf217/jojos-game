package MorenoJoel_JojoGachaAdventure;


/**
 * Clase que contiene todas las variables de tu personaje
 * (no está en uso)
 * @author Joel Moreno
 *
 */
public class Jotaro extends Character implements levelUp{
	
	/**
	 * Experiencia máxima acorde al nivel del personaje
	 */
	int maxXP = 10;
	/**
	 * Experiencia actual del personaje
	 */
	int actualXP = 0;
	/**
	 * Nivel del personaje
	 */
	int level = 1;
	/**
	 * Singleton
	 */
	private static Jotaro instance = null;
	/**
	 * Constructor Jotaro
	 */
	private Jotaro(String name, int x1, int y1, String sprites, String charName, String standName, int maxHP,
			int actualHP, int atk) {
		super(name, x1, y1, sprites, charName, standName, maxHP, actualHP, atk);
		
	}
	/**
	 * Cuando se acceda al singleton, si Jotaro no existe, lo crea, de no ser así, simplemente lo retorna
	 */
	public static Jotaro getInstance(String name, int x1, int y1, String sprites, String charName, String standName, int maxHP,
			int actualHP, int atk) {
		if (instance == null) {
			instance = new Jotaro(name, x1, y1, sprites, charName, standName, maxHP, actualHP, atk);
		}return instance;
	}
	
	/**
	 * Para obtener la información de Jotaro
	 */
	public static Jotaro getInstance() {
		return instance;
	}
	/**
	 * Función para subir de nivel
	 */
	public void level(int obtainedXP) throws InterruptedException {
		
		this.actualXP+=obtainedXP;		
		
		if (this.actualXP >= this.maxXP) {
			this.actualXP=Math.abs(actualXP-maxXP);
			this.level++;
			this.maxXP*=1.5;
			this.maxHP*=1.2;
			this.actualHP=this.maxHP;
			this.atk*=1.15;			
			
			System.out.println("Tu nivel ahora es: "+this.level);
			System.out.println("Para subir de nivel necesitas: "+this.maxXP);
			System.out.println("Tu vida máxima ahora es: "+this.maxHP);
			System.out.println("Tu ataque ahora es: "+this.atk);
			//recursividad por si consigues más xp de la cuenta
			level(obtainedXP);
		}
		System.out.println("xp actual es: "+this.actualXP);

	}	

	@Override
	public int gainExp() {
		return 0;		
		
	}
	
}

